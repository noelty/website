---
path: /events/foss-era-3.0/
date: '2024-04-5'
datestring: '5-6 April 2024'
author: 'noelty'
title: 'FOSS Era 3.0'
cover: './foss-era-3.0.jpg'
name: 'Noel Tom Santhosh'
---

![Poster](./foss-era-3.0.jpg)

FOSSNSS presents _#FOSSEra 3.0: Learn, Collaborate, Innovate!#_

Third edition of our annual event - FOSSEra 3.0, a dynamic event where learning meets innovation in the world of open-source technology.

🔹 Workshop: Exploring Docker and Podman

🗓️ Date: April 5th, 2024

🕙 Time: 10:00 AM - 4:00 PM

🔹 Hackfiesta v3.0: An Unforgettable Overnight Hackathon Experience!

🟥Open to all colleges

🗓️ Date: April 5th (5:00 PM) - April 6th (4:00 PM) 2024

💸Prize pool : 10k
