---
path: /events/MiniDebConf-Palakkad/
date: "2022-11-12"
datestring: "12-13 November 2022"
cover: "./mainpic.jpg"
author: "sivharinair"
name: "Siv Hari Nair"
title: "MiniDebConf Palakkad"
---

![Poster](./posterintro.png)

__MINIDEBCONF__ 

The Debian India community, in collaboration with the FOSSNSS team, organised a Mini Debian Conference at the NSS College of Engineering in Palakkad, Kerala, India, as a pre-event to the Annual Debian Conference, which will be held in India in 2023. It took place on November 12 and 13, 2022. MiniDebConf Palakkad 2022 is a scaled-down version of the Debian Conference that emphasises Debian as free and open-source software through presentations and workshops. The conference served as a venue for advancements in not only Debian, but also in a variety of other Free Software projects. The event drew a broad mix of participants from various levels of skill and locations around the world, with people travelling from Nepal and Tamil Nadu to attend.
The broad group of people in attendance at the MiniDebConf increased the event's mood tenfold. 17 talks and 6 workshops were successfully held in three venues over the course of two days.

![Poster](./mdcgroup.jpg)

🗓️ **DATE and TIME:** Nov 12 2022 09:00 AM - Nov 13 2022 05:30 PM

<h2><u>Organizing Committee</u></h2>
<p>Chief Patron: Sri G. Sukumaran Nair</p>
<p>Patron: Dr. P.R Suresh</p>
<p>Chairperson: Dr. Maya Mohan</p>
<p>Program Convenor: Dr. Anuraj Mohan</p>
<h3><u>Student Team</u></h3>
<p>Coordinator: Jayaraj J</p>
<p>Events Head: Ananthu V and Sonika Rajesh Pillai</p>
<p>Treasurer: Rasica K Nambiar</p>


<h3><u>Sponsors</u></h3>
<p><u>Platinum Sponsor</u></p>
<ul>
<li>	IIT Palakkad Technology and IHub Foundation</li>
</ul>
<p><u>	Gold Sponsors</u></p>
<ul>
<li>	Frappe</li>
<li>	FOSS UNITED</li>
</ul>
<p><u>	Bronze Sponsor</p></u>
<ul>
<li>	MEDIWAVE</li>
</ul>



