---
path: /events/servercraft/
date: '2024-03-22'
datestring: '22 March 2024'
author: 'noelty'
title: 'Servercraft Workshop'
cover: './servercraft.jpeg'
name: 'Noel Tom Santhosh'
---

![Poster](./servercraft.jpeg)

Embark on a transformative journey of skill enhancement with our exclusive workshop: Servercraft: Journey into Administration Mastery, presented by STACS in collaboration with FOSS Cell, NSSCE!!

Tap into the wealth of knowledge and experience shared by seasoned professionals **Kannan V M** and **Ambady Anand S**.

> Date:22.03.2024<br>
> Time:10:00 AM - 4:00 PM
